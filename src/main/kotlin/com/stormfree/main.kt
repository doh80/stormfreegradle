package com.stormfree

fun main(args: Array<String>) {

    /*
    1. Write a function (and necessary classes) that takes in two integer parameters for hour and minute,
        then returns the integer calculation of the narrowest positive angle of degrees between the two hands of an analog clock.
        For example, 6:00 would return 180. Pay close attention to edge cases and handle any data errors appropriately with clear exceptions.

    2. Write some JUnit tests for the previous function to appropriately cover important cases.

    3. Create a static executable com.main file that will pass command line parameters into the calculation function and print the result or exception details.
        For the 6:00 example we expect "java -jar app.jar 6 00" to print 180.

    4. Create a gradle (or maven) project that supports the test command to run all the unit tests above, and the build command to create the "app.jar" file at the root folder of the project.
    * */

    if (args.size != 2) {
        println("You have to enter two integer parameters for hour and minute")

    } else {
        val clockAngleFunction = ClockAngleFunction()
        println(clockAngleFunction.getClockAngle(args[0], args[1])) // it takes string values
    }

//     val clockAngleFunction = ClockAngleFunction()
//     println(clockAngleFunction.getClockAngle(345, 0)) // you can pass int values, it will return -1.0 if error case happened

}