package com.stormfree

import java.lang.Exception

class ClockAngleFunction {

    private val ONE_HOUR_ANGLE = 30.0
    private val ONE_MIN_ANGLE = 6.0

    public fun getClockAngle(hourStr : String, minStr : String) : String {
        return try {
            val hourInt = hourStr.toInt()
            val minInt = minStr.toInt()

            if (hourStr.length > 2 || hourInt !in 1..12)  {
                throw IllegalArgumentException("Hour value must be the number between 1 to 12, and must be 1 or 2 digit length.")

            } else if (minStr.length > 2 || minInt !in 0..59) {
                throw IllegalArgumentException("Minute value must be the number between 0 to 59, and must be 1 or 2 digit length.")

            }

            getClockAngle(hourInt, minInt).toString()
        } catch (e : Exception) {
            e.toString()
        }
    }

    // return -1.0 for error
    public fun getClockAngle(hour: Int, min: Int) : Double {
        var angle = 0.0

        try {
            if (hour !in 1..12)  {
                throw IllegalArgumentException("Hour value must be the number between 1 to 12")

            } else if (min !in 0..59) {
                throw IllegalArgumentException("Minute value must be the number between 0 to 59")
            }
        } catch (e : Exception) {
            println(e.toString())
            return -1.0
        }

        if (isHourAndMinOverlapped(hour.toDouble(), min.toDouble())) {
            return angle

        } else {
            val hourAngle = getHourAngle(hour, min)
            val minAngle = getMinAngle(min)

            val big = maxOf(hourAngle, minAngle)
            val small = minOf(hourAngle, minAngle)

            angle = big - small
        }
        angle %= 360

        if (angle > 180) {
            angle = 360 - angle //to get narrowest positive angle
        }

        return angle
    }

    private fun getHourAngle(hour: Int, min: Int) : Double {
        var hourValue = hour
        if (hourValue == 12) {
            hourValue = 0
        }
        return (ONE_HOUR_ANGLE * hourValue) + (0.5 * min)
    }

    private fun getMinAngle(min: Int) : Double {
        return ONE_MIN_ANGLE * min
    }

    //only 0:00 for this case (there is one other case is between 3:16 and 3:17)
    private fun isHourAndMinOverlapped(hour: Double, min: Double) : Boolean {
        return min == (60.0 / 11.0) * hour
    }

}